#!/usr/bin/env python3

import os
from jinja2 import FileSystemLoader, Environment, PackageLoader, select_autoescape

loader = FileSystemLoader(os.path.dirname(os.path.abspath(__file__)))
env = Environment(loader=loader)
template = env.get_template('.gitlab-ci.yml.jinja')
rendered = template.render()
# Remove empty/whitepsace-only lines
rendered = os.linesep.join([s for s in rendered.splitlines() if s.strip()])
with open('.gitlab-ci.yml', 'w') as f:
    f.write(rendered)
